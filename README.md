# Bgchallenge
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

Run 'npm install' to install the required packages.
Run 'npm start' to run the app and listen to  `http://localhost:4200/`.

IDE used : WebStorm (https://www.jetbrains.com/webstorm)

Bootstrap is used for some components. The general UI is very simple and user friendly
Due to limited time tests are not implemented.

The functionality is working as expected.


Additional info:

There is an error in the given json files.
Although the city in the URLs are correct, the results for San_Francisco and Detroit are wrong. They display data for Los_Angeles and New_York instead.

San_Francisco (display data for) --> Los_Angeles
Detroit (display data for) --> New_York

The rest of the cities works ok.