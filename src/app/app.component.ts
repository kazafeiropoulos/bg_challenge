import {Component} from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent{

  showStyle: false;

  setWeatherStyle() {
    if(this.showStyle) {
      return "block";
    } else {
      return "none";
    }
  }

  items: any[] = [
    { cityname: "New York" },
    { cityname: "San Francisco" },
    { cityname: "Chicago" },
    { cityname: "Los Angeles" },
    { cityname: "Detroit" }];

  constructor(public http: HttpClient) {}

  req:any;
  private errorMsg: string;

  data: Object;
  loading: boolean;
  city: string;
  term: any;

  makeRequest(city) {
    this.loading = true;
    this.city = city;

    switch (city) {
      case 'New York':
        this.req = "NY/New_York.json";
        break;

      case 'San Francisco':
        this.req = "CA/San_Francisco.json";
        break;

      case 'Chicago':
        this.req = "IL/Chicago.json";
        break;

      case 'Los Angeles':
        this.req = "CA/Los_Angeles.json";
        break;

      case 'Detroit':
        this.req = "MI/Detroit.json";
        break;
    }

    this.http.get('http://api.wunderground.com/api/ab5ff0e58f32c6cf/history_20171030/q/'+this.req)
        .subscribe(
        res => {
          this.data = res;
          console.log(this.data);
          this.loading = false;
        },
        err => {
          console.log("Error occured.")
        });
    }
}
